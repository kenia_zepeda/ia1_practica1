import random
from datetime import datetime

from flask import Flask, render_template, request
import pandas as pd

app = Flask(__name__)

# CONSTANTES DEL ALGORITMO
maximo_generaciones = 2000  # Número máximo de generaciones que va a tener el algoritmo
modoSeleccion = '1'
criterio = '1'
file = ""
tamPoblacion = 20
mejorIndividuo = []
mensaje = ""


@app.route('/')
def index():
    return render_template("index.html", mensaje=('', '', ''))


@app.route('/analizar', methods=['POST', 'GET'])
def analizar():
    global file
    f = request.files['file']
    file = f.filename

    params = request.form
    global criterio
    criterio = params['criterio']
    global modoSeleccion
    modoSeleccion = params['modo']

    print("Algoritmo corriendo")

    printHtml("Algoritmo corriendo")

    generacion = 0
    poblacion = inicializarPoblacion()
    fin = verificarCriterio(poblacion, generacion)

  #  print('*************** GENERACION ', generacion, " ***************")
    printHtml('*************** GENERACION '+ str(generacion) + "***************")
    imprimirPoblacion(poblacion)

    while (fin == None):
        padres = seleccionarPadres(poblacion)
        poblacion = emparejar(padres)
        generacion += 1  # Lo pongo aquí porque en teoría ya se creó una nueva generación
        fin = verificarCriterio(poblacion, generacion)

       # print('*************** GENERACION ', generacion, " ***************")
        printHtml('*************** GENERACION ' + str(generacion) + "***************")
        imprimirPoblacion(poblacion)

    # Obtengo la mejor solución y la muestro
    arregloMejorIndividuo = sorted(poblacion, key=lambda item: item.fitness, reverse=False)[
                            :1]  # Los ordena de menor a mayor

    global mejorIndividuo
    mejorIndividuo = arregloMejorIndividuo[0]

    print('\n\n*************** MEJOR INDIVIDUO***************')
    print('Individuo: ', mejorIndividuo.solucion, ' Fitness: ', mejorIndividuo.fitness)
    printHtml('\n*************** MEJOR INDIVIDUO***************')
    printHtml('Individuo: ' + str(mejorIndividuo.solucion) + ' Fitness: ' + str(mejorIndividuo.fitness))
    escribirBitacora(generacion, mejorIndividuo.solucion )

    global mensaje
    return render_template("index.html", mensaje=(mensaje, '', ''))


@app.route('/calcularNota', methods=['POST', 'GET'])
def calcularNota():
    nota = 0
    params = request.form
    global  mejorIndividuo
    p1 = float(params['nota1'])
    p2 = float(params['nota2'])
    p3 = float(params['nota3'])
    p4 = float(params['nota4'])
    p1 = p1 * mejorIndividuo.solucion[0]
    p2 = p2 * mejorIndividuo.solucion[1]
    p3 = p3 * mejorIndividuo.solucion[2]
    p4 = p4 * mejorIndividuo.solucion[3]
    nota = p1 + p2 + p3 + p4
    print("Nota calculada: " + str(nota))
    global mensaje
    mensaje = "\nLa nota final podria ser: " + str(nota)
    return render_template("index.html", mensaje=(mensaje, '', ''))


def printHtml(texto):
    global mensaje
    mensaje += texto + "\n"


def inicializarPoblacion():
    poblacion = []
    for x in range(tamPoblacion):
        lista = listaAleatorios()
        individuo = Nodo(lista, evaluarFitness(lista))
        poblacion.append(individuo)

    return poblacion  # Retorno la población ya creada


def listaAleatorios():
    lista = [0] * 4
    for i in range(4):
        rand = round(random.uniform(-1, 1), 3)
        lista[i] = rand
    return lista


def imprimirPoblacion(poblacion):
    for individuo in poblacion:
        # print('Individuo: ', individuo.solucion, ' Valor de x: ', individuo.x, ' Fitness: ', individuo.fitness)
        #print('Individuo: ', individuo.solucion, ' Fitness: ', individuo.fitness)
        printHtml('Individuo: ' + str(individuo.solucion) + ' Fitness: ' + str(individuo.fitness))


def evaluarFitness(ls):
    df = load_file(file)

    df['M1'] = df['PROYECTO 1'] * ls[0]
    df['M2'] = df['PROYECTO 2'] * ls[1]
    df['M3'] = df['PROYECTO 3'] * ls[2]
    df['M4'] = df['PROYECTO 4'] * ls[3]
    df['NC'] = df['M1'] + df['M2'] + df['M3'] + df['M4']
    df['E'] = df['NOTA FINAL'] - df['NC']
    df['E2'] = df['E'] ** 2

    sum = df['E2'].sum()
    n = df.shape[0]
    fit = sum / n
    fit = round(fit, 2)

    return fit


def load_file(file):
    return pd.read_csv(
        file,
        dtype={'PROYECTO 1': 'float64', 'PROYECTO 2': 'float64', 'PROYECTO 3': 'float64', 'PROYECTO 4': 'float64',
               'NOTA FINAL': 'float64'}
    )


def verificarCriterio(poblacion, generacion):
    # Calculo el valor fitness de los individuos
    for individuo in poblacion:
        individuo.fitness = evaluarFitness(individuo.solucion)

    if criterio == '1':
        return criterio1(generacion)
    elif criterio == '2':
        return criterio2(poblacion, generacion)
    else:
        return criterio3(poblacion, generacion)


def criterio1(generacion):
    #print("*******criterio1()*******")
    # Mejor fitness de los individuos por numero maximo de generaciones
    if generacion >= maximo_generaciones:
        return True
    else:
        return None


def criterio2(poblacion, generacion):
    #print("*******criterio2()*******")
    # Mejor fitness de los individuos
    for individuo in poblacion:
        if individuo.fitness < 1:
            return True

    if generacion >= maximo_generaciones:
        return True
    else:
        return None


def criterio3(poblacion, generacion):
    #print("*******criterio3()*******")
    sum = 0
    for individuo in poblacion:
        sum += individuo.fitness

    promedio = sum / len(poblacion)
    print("El fitness promedio es: " + str(promedio))

    for individuo in poblacion:
        if individuo.fitness >= (promedio - 100) and individuo.fitness <= (promedio + 100):
            return True

    if generacion >= maximo_generaciones:
        return True
    else:
        return None


def seleccionarPadres(poblacion):
    if modoSeleccion == '1':
        return seleccion1(poblacion)
    elif modoSeleccion == '2':
        return seleccion2(poblacion)
    else:
        return seleccion3(poblacion)


def seleccion1(poblacion):
    # Aleatorio
    #print("*******seleccion1()*******")
    padres = []
    global tamPoblacion
    m = tamPoblacion / 2
    m = round(m)
    for x in range(m):
        i = random.randint(0, tamPoblacion - 1)
        padres.append(poblacion[i])

    return padres


def seleccion2(poblacion):
    # Padres con mejor valor fitness
    #print("*******seleccion2()*******")
    global tamPoblacion
    mitad = round(tamPoblacion / 2)
    padres = sorted(poblacion, key=lambda item: item.fitness, reverse=False)[:mitad]

    return padres


def seleccion3(poblacion):
    # Padres pares
    #print("*******seleccion3()*******")
    posPar = 0

    padres = []
    global tamPoblacion

    while posPar < tamPoblacion:
        padres.append(poblacion[posPar])
        posPar += 2

    return padres


def emparejar(padres):
    global tamPoblacion
    hijos = []
    h1 = Nodo()
    tampadres = len(padres)


    for i in range(tampadres, tamPoblacion):
        index1 = random.randint(0, tampadres - 1)
        index2 = random.randint(0, tampadres - 1)
        while (index1 == index2):
            index2 = random.randint(0, tampadres - 1)

        h1 = Nodo()
        h1.solucion = cruzar(padres[index1].solucion, padres[index2].solucion)
        h1.solucion = mutar(h1.solucion)
        hijos.append(h1)

    for hijo in hijos:
        padres.append(hijo)

    return padres


def cruzar(solucionPadre1, solucionPadre2):
    solucionHijo = []

    ran = random.random()
    if ran <= .6:
        solucionHijo.append(solucionPadre1[0])
    else:
        solucionHijo.append(solucionPadre2[0])

    ran = random.random()
    if ran <= .6:
        solucionHijo.append(solucionPadre1[1])
    else:
        solucionHijo.append(solucionPadre2[1])

    ran = random.random()
    if ran <= .6:
        solucionHijo.append(solucionPadre1[2])
    else:
        solucionHijo.append(solucionPadre2[2])

    ran = random.random()
    if ran <= .6:
        solucionHijo.append(solucionPadre1[3])
    else:
        solucionHijo.append(solucionPadre2[3])

    return solucionHijo


def mutar(solucionHijo):
    ran = random.random()
    if ran <= .5:
        pos = random.randint(0, 3)
        valrand = random.uniform(-2, 2)
        valrand = round(valrand, 3)
        solucionHijo[pos] = valrand

    return solucionHijo


def ejecutar():
    print("Algoritmo corriendo")

    generacion = 0
    poblacion = inicializarPoblacion()
    fin = verificarCriterio(poblacion, generacion)

    print('*************** GENERACION ', generacion, " ***************")
    imprimirPoblacion(poblacion)

    while (fin == None):
        padres = seleccionarPadres(poblacion)
        poblacion = emparejar(padres)
        generacion += 1  # Lo pongo aquí porque en teoría ya se creó una nueva generación
        fin = verificarCriterio(poblacion, generacion)

        print('*************** GENERACION ', generacion, " ***************")
        printHtml('*************** GENERACION ', generacion, " ***************")
        imprimirPoblacion(poblacion)

    # Obtengo la mejor solución y la muestro
    arregloMejorIndividuo = sorted(poblacion, key=lambda item: item.fitness, reverse=False)[
                            :1]  # Los ordena de menor a mayor

    global mejorIndividuo
    mejorIndividuo = arregloMejorIndividuo[0]

    print('\n\n*************** MEJOR INDIVIDUO***************')
    print('Individuo: ', mejorIndividuo.solucion, ' Fitness: ', mejorIndividuo.fitness)


def escribirBitacora(generaciones, solucion):
    criterioFin = ""
    modoSel = ""

    if criterio == '1':
        criterioFin = "Máximo número de generaciones"
    elif criterio == '2':
        criterioFin = "Mejor fitness (minimo valor fitness)"
    else:
        criterioFin = "Un valor fitness promedio dentro de la población"


    if modoSeleccion == '1':
        modoSel = "Selección aleatoria"
    elif modoSeleccion == '2':
        modoSel = "Selección de los padres con mejor valor fitness"
    else:
        modoSel = "Selección de padres solo en posiciones pares"

    print("Escribiendo en bitacora")
    f = open("bitacora.txt", "a")
    tiempo = datetime.now()

    contenido = [

        'Fecha de ejecución: ' + tiempo.date().isoformat() + '\n',
        'Hora de ejecución: ' + tiempo.time().isoformat() + '\n',

        'Nombre del documento utilizado: ', file + '\n',
        'Criterio de finalización utilizado: ', criterioFin + '\n',
        'Criterio de selección de padres: ', modoSel, '\n',
        'No. Generaciones: ', str(generaciones) + '\n',
        'Mejor solucion: ', str(solucion) + '\n',
        '------------------------------\n',
    ]
    f.writelines(contenido)
    f.close()

    return 0


class Nodo:
    # solucion = []
    # fitness = 0 #Valor fitness
    # x = 0 #Para la tarea se guarda el valor de x

    # Le defino parámetros al constructor y le pongo valores por defecto por si no se envían
    def __init__(self, poblacion=[], fitness=0, x=0):
        self.solucion = poblacion
        self.fitness = fitness
        self.x = x


"""
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9090, debug=True)"""
